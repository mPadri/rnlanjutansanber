import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  StatusBar,
  TouchableOpacity,
  TextInput,
  Modal,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import storage from '@react-native-firebase/storage';

const Register = ({navigation}) => {
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState(null);

  const toggleCamera = () => {
    setType(type === 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    // configurasi take picture
    const options = {quality: 0.5, bas64: true};
    // take picture
    if (camera) {
      const data = await camera.takePictureAsync(options);
      //   console.log('isi take picture:', data);
      setPhoto(data);
      setIsVisible(false);
    }
  };

  const uploadImage = (uri) => {
    const sessionId = new Date().getTime();
    return storage()
      .ref(`image/${sessionId}`)
      .putFile(uri)
      .then((res) => {
        alert('Upload success');
      })
      .catch((err) => {
        alert(err);
      });
  };

  return (
    <>
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{flex: 1}}
            ref={(ref) => {
              camera = ref;
            }}
            type={type}>
            <View>
              <TouchableOpacity
                style={{
                  backgroundColor: '#fff',
                  width: 50,
                  height: 50,
                  borderRadius: 50,
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: 15,
                }}
                onPress={() => toggleCamera()}>
                <MaterialCommunityIcons name="rotate-3d-variant" size={25} />
              </TouchableOpacity>
            </View>
            <View style={{flex: 1}} />
            <View
              style={{
                borderWidth: 1,
                borderColor: '#fff',
                width: 150,
                height: 100,
                alignSelf: 'center',
              }}
            />
            <View style={{flex: 1}} />
            <View style={{alignItems: 'center', marginBottom: 20}}>
              <TouchableOpacity
                style={{
                  backgroundColor: '#fff',
                  width: 50,
                  height: 50,
                  borderRadius: 50,
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: 15,
                }}
                onPress={() => takePicture()}>
                <Ionicons name="camera" size={25} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
      <View style={{flex: 1}}>
        <StatusBar backgroundColor="#3ec6ff" />
        <View
          style={{
            backgroundColor: '#3ec6ff',
            height: 200,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={
              photo == null
                ? require('../../assets/image/user.png')
                : {uri: photo.uri}
            }
            style={{width: 100, height: 100, borderRadius: 50}}
          />
          <TouchableOpacity
            style={{
              marginVertical: 15,
            }}
            onPress={() => setIsVisible(true)}>
            <Text style={{fontWeight: 'bold', color: 'white', fontSize: 16}}>
              Change Picture
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{alignItems: 'center', marginTop: -20}}>
          <View
            style={{
              backgroundColor: 'white',
              height: 300,
              width: Dimensions.get('window').width - 30,
              borderRadius: 10,
              elevation: 2,
            }}>
            <View style={{padding: 15}}>
              <Text>Nama</Text>
              <TextInput underlineColorAndroid="#d4d4d4" placeholder="Nama" />
              <Text>Email</Text>
              <TextInput
                underlineColorAndroid="#d4d4d4"
                placeholder="Email"
                keyboardType="email-address"
              />
              <Text>Password</Text>
              <TextInput
                underlineColorAndroid="#d4d4d4"
                secureTextEntry={true}
                placeholder="Password"
              />

              <TouchableOpacity
                style={{
                  backgroundColor: '#3EC6FF',
                  padding: 10,
                  elevation: 2,
                  marginTop: 15,
                }}
                onPress={() => uploadImage(photo.uri)}>
                <Text
                  style={{
                    textAlign: 'center',
                    color: '#fff',
                    fontWeight: 'bold',
                  }}>
                  REGISTER
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </>
  );
};

export default Register;
