import React, {useEffect} from 'react';
import {Image, StyleSheet, View, StatusBar} from 'react-native';

const SplashScreen = ({navigation}) => {
  // useEffect(() => {
  //   setTimeout(() => {
  //     navigation.replace('Intro');
  //   }, 3000);
  // }, []);
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor="#fff" />
      <Image
        source={require('../../assets/image/logo.jpg')}
        style={{width: '100%', height: 200}}
      />
    </View>
  );
};

export default SplashScreen;
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
