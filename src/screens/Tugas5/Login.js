import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Pressable,
  StatusBar,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import {API} from '../../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth';
import TouchID from 'react-native-touch-id';

const config = {
  title: 'Authentication Required', // Android
  imageColor: '#191970', // Android
  imageErrorColor: 'red', // Android
  sensorDescription: 'Touch sensor', // Android
  sensorErrorDescription: 'Failed', // Android
  cancelText: 'Cancel', // Android
  fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
  unifiedErrors: true, // use unified error messages (default false)
  passcodeFallback: true,
};

const optionalConfigObject = {
  unifiedErrors: true, // use unified error messages (default false)
  passcodeFallback: true, // if true is passed, itwill allow isSupported to return an error if the device is not enrolled in touch id/face id etc. Otherwise, it will just tell you what method is supported, even if the user is not enrolled.  (default false)
};

const Login = ({navigation}) => {
  useEffect(() => {
    configureGoogleSignIn();
  }, []);

  // login Finger Print
  const signInWithFingerPrint = () => {
    // console.log('touch:', TouchID);
    TouchID.authenticate('', config)
      .then((success) => {
        // alert('success');
        navigation.replace('Profile');
      })
      .catch((error) => {
        alert('error');
        console.log(error);
      });

    //   TouchID.isSupported(optionalConfigObject)
    //     .then((biometryType) => {
    //       // Success code
    //       if (biometryType === 'FaceID') {
    //         console.log('FaceID is supported.');
    //       } else {
    //         console.log('TouchID is supported.');
    //       }
    //     })
    //     .catch((error) => {
    //       // Failure code
    //       console.log(error.messagex);
    //     });
  };

  // login with google
  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      webClientId:
        '5129955728-ev18qjsuea90qtsem0ncdr9e0hl3q30s.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
    });
  };

  // Bagian dari JWT
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const saveToken = async (token) => {
    // simpan token di local storage
    try {
      await AsyncStorage.setItem('token', token);
    } catch (err) {
      console.log(err);
    }
  };

  // login JWT

  // const onLogin = () => {
  //   let data = {
  //     email: email,
  //     password: password,
  //   };

  //   Axios.post(`${API}/login`, data, {
  //     timeout: 20000,
  //   })
  //     .then((res) => {
  //       console.log('login -> ', res.data);
  //       saveToken(res.data.token);
  //       navigation.replace('Profile');
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // };

  // login google

  const signIn = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();
      // console.log('isi idToken->', idToken);
      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth().signInWithCredential(credential);
      navigation.replace('Profile');
    } catch (error) {
      console.log(error);
    }
  };

  // login with email & password firebase
  const onLogin = () => {
    return auth()
      .signInWithEmailAndPassword(email, password)
      .then((res) => {
        // navigation.replace('MainApp');
        navigation.reset({
          index: 0,
          routes: [{name: 'MainApp'}],
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor="#fff" />
      <Image
        source={require('../../assets/image/logo.jpg')}
        style={{width: '100%', height: 200}}
      />
      <View style={styles.wrapperForm}>
        <Text>Username</Text>
        <TextInput
          underlineColorAndroid="#c6c6c6"
          placeholder="Username or Email"
          value={email}
          onChangeText={(email) => setEmail(email)}
        />
        <View style={{height: 10}} />
        <Text>Password</Text>
        <TextInput
          underlineColorAndroid="#c6c6c6"
          placeholder="Password"
          secureTextEntry={true}
          value={password}
          onChangeText={(password) => setPassword(password)}
        />
      </View>
      <Pressable
        style={styles.btnLogin}
        android_ripple={{color: '#fff'}}
        onPress={() => onLogin()}>
        <Text style={styles.text}>LOGIN</Text>
      </Pressable>
      <View
        style={{
          flexDirection: 'row',
          marginVertical: 5,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View style={{height: 2, backgroundColor: '#d4d4d4', width: 145}} />
        <Text style={{marginHorizontal: 5}}>OR</Text>
        <View
          style={{
            height: 2,
            backgroundColor: '#d4d4d4',
            width: 145,
          }}
        />
      </View>

      <GoogleSigninButton
        style={{width: 198, height: 55, alignSelf: 'center', marginBottom: 10}}
        size={GoogleSigninButton.Size.Wide}
        color={GoogleSigninButton.Color.Dark}
        onPress={() => signIn()}
      />

      <Pressable
        style={styles.btnLoginFinger}
        android_ripple={{color: '#fff'}}
        onPress={() => signInWithFingerPrint()}>
        <Text style={styles.text}>LOGIN FINGERPRINT</Text>
      </Pressable>

      <View style={{flex: 1}} />
      <View
        style={{
          marginBottom: 30,
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'row',
        }}>
        <Text>Belum Mempunyai Akun ?</Text>
        <Pressable onPress={() => navigation.navigate('Register')}>
          <Text style={{marginLeft: 5, color: '#191970'}}>Buat Akun</Text>
        </Pressable>
      </View>
    </View>
  );
};

export default Login;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  wrapperForm: {
    margin: 15,
  },
  btnLogin: {
    backgroundColor: '#3EC6FF',
    marginHorizontal: 20,
    padding: 10,
    elevation: 2,
  },
  text: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: '#fff',
  },
  btnLoginGoogle: {
    backgroundColor: '#ff5c5c',
    marginHorizontal: 20,
    padding: 10,
    elevation: 2,
  },
  btnLoginFinger: {
    backgroundColor: '#191970',
    marginHorizontal: 20,
    padding: 10,
    elevation: 2,
  },
});
