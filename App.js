import React, {useState, useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
// import Intro from './src/screens/Tugas1/Intro';
// import Biodata from './src/screens/Tugas2/Biodata';
// import TodoList from './src/screens/Tugas3/TodoList';
// import Context from './src/screens/Tugas4';
import SplashScreen from './src/screens/Tugas5/SplashScreen';
import {NavigationContainer} from '@react-navigation/native';
// import Navigation from './src/screens/Tugas5/Navigation';
import firebase from '@react-native-firebase/app';
import Navigation from './src/navigation';
import AsyncStorage from '@react-native-community/async-storage';

let firebaseConfig = {
  apiKey: 'AIzaSyAD9VF2mWA-Qb7fPtsAgfzl7wc5cpl290w',
  authDomain: 'myproject-1fa7b.firebaseapp.com',
  databaseURL: 'https://myproject-1fa7b.firebaseio.com',
  projectId: 'myproject-1fa7b',
  storageBucket: 'myproject-1fa7b.appspot.com',
  messagingSenderId: '5129955728',
  appId: '1:5129955728:web:7105fc75741e917704eb15',
};
// Inisialisasi firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [status, setStatus] = useState(null);
  const [token, setToken] = useState(null);

  //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
  useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);
    async function getStatus() {
      try {
        const installed = await AsyncStorage.getItem('skip');
        // console.log('getStatus -> installed', installed);
        return setStatus(installed);
      } catch (err) {
        console.log(err);
      }
    }
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('token');
        console.log('getToken -> token', token);

        return setToken(token);
      } catch (err) {
        console.log(err);
      }
    }
    getToken();
    getStatus();
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }

  return (
    // <View>
    //   {/* <Intro /> */}
    //   {/* <Biodata /> */}
    //   {/* <TodoList /> */}
    //   {/* <Context /> */}
    // </View>
    // <NavigationContainer>
    //   <Navigation />
    // </NavigationContainer>

    <NavigationContainer>
      <Navigation status={status} token={token} />
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default App;
