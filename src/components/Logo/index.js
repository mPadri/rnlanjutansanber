import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Logo = ({logo, namaLogo, type, onPress}) => {
  const Icon = () => {
    if (type == 'Ionicons') {
      return <Ionicons name={logo} size={42} color="#fff" />;
    }
    if (type == 'MaterialCommunityIcons') {
      return <MaterialCommunityIcons name={logo} size={42} color="#fff" />;
    }
    if (type == 'Image') {
      return <Image source={logo} style={{width: 43, height: 43}} />;
    }
  };
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.logo}>
        <Icon />
        <Text style={styles.titleLogo}>{namaLogo}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Logo;
const styles = StyleSheet.create({
  logo: {
    alignItems: 'center',
  },
  titleLogo: {
    color: '#fff',
    fontSize: 12,
    marginTop: 5,
  },
});
