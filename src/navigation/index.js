import React, {useEffect, useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../screens/Home';
import Profile from '../screens/Profile';
import Maps from '../screens/Maps';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import {Text} from 'react-native';
import Chart from '../screens/Chart';
import Chatting from '../screens/Chatting';
import Login from '../screens/Login';
import Intro from '../screens/Intro';
import AsyncStorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator tabBarOptions={{keyboardHidesTabBar: true}}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({focused}) => (
            <MaterialCommunityIcons
              name="home-variant-outline"
              size={24}
              style={focused ? {color: '#3EC6FF'} : {color: 'grey'}}
            />
          ),
          tabBarLabel: ({focused}) => (
            <Text
              style={
                focused
                  ? {color: '#3EC6FF', fontSize: 11}
                  : {color: 'grey', fontSize: 11}
              }>
              Home
            </Text>
          ),
        }}
      />

      <Tab.Screen
        name="Maps"
        component={Maps}
        options={{
          tabBarIcon: ({focused}) => (
            <Entypo
              name="location"
              size={24}
              style={focused ? {color: '#3EC6FF'} : {color: 'grey'}}
            />
          ),
          tabBarLabel: ({focused}) => (
            <Text
              style={
                focused
                  ? {color: '#3EC6FF', fontSize: 11}
                  : {color: 'grey', fontSize: 11}
              }>
              Maps
            </Text>
          ),
        }}
      />
      <Tab.Screen
        name="Chatting"
        component={Chatting}
        options={{
          tabBarIcon: ({focused}) => (
            <Entypo
              name="chat"
              size={24}
              style={focused ? {color: '#3EC6FF'} : {color: 'grey'}}
            />
          ),
          tabBarLabel: ({focused}) => (
            <Text
              style={
                focused
                  ? {color: '#3EC6FF', fontSize: 11}
                  : {color: 'grey', fontSize: 11}
              }>
              Chatting
            </Text>
          ),
        }}
      />

      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: ({focused}) => (
            <Feather
              name="user"
              size={24}
              style={focused ? {color: '#3EC6FF'} : {color: 'grey'}}
            />
          ),
          tabBarLabel: ({focused}) => (
            <Text
              style={
                focused
                  ? {color: '#3EC6FF', fontSize: 11}
                  : {color: 'grey', fontSize: 11}
              }>
              Profile
            </Text>
          ),
        }}
      />
    </Tab.Navigator>
  );
};
const Navigation = ({status, token}) => {
  console.log('Navigation -> token', token);
  // console.log('Navigation -> status', status);

  return (
    <Stack.Navigator
      initialRouteName={
        status && token ? 'MainApp' : status ? 'Login' : 'Intro'
      }>
      <Stack.Screen
        name="Intro"
        component={Intro}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Chart"
        component={Chart}
        options={{headerTitle: 'React Native'}}
      />
    </Stack.Navigator>
  );
};

export default Navigation;
