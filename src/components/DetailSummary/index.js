import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const DetailSummary = ({title, today, total}) => {
  return (
    <>
      <View style={{padding: 5}}>
        <Text style={styles.titleHeader}>{title}</Text>
      </View>
      <View style={styles.wrapperDeskripsi}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.title}>Today</Text>
          <Text style={styles.title}>{today} Orang</Text>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.title}>Total</Text>
          <Text style={styles.title}>{total} Orang</Text>
        </View>
      </View>
    </>
  );
};

export default DetailSummary;
const styles = StyleSheet.create({
  titleHeader: {
    color: '#fff',
    fontWeight: 'bold',
  },
  title: {
    color: '#fff',
  },
  wrapperDeskripsi: {
    backgroundColor: '#088dc4',
    padding: 5,
    paddingVertical: 10,
    paddingHorizontal: 35,
  },
});
