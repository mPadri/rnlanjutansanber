import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';
import {GiftedChat} from 'react-native-gifted-chat';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';

const Chatting = () => {
  const [messages, setMessages] = useState([]);
  const [user, setUser] = useState({});
  useEffect(() => {
    const user = auth().currentUser;
    // console.log('current user ->', user);
    setUser(user);
    getDataMessages();
    return () => {
      const db = database().ref('messages');
      if (db) {
        db.off();
      }
    };
  }, []);

  //   kirim pesan
  const onSend = (messages = []) => {
    // console.log('isi param message->', messages);
    for (let i = 0; i < messages.length; i++) {
      database().ref('messages').push({
        _id: messages[i]._id,
        createdAt: database.ServerValue.TIMESTAMP,
        text: messages[i].text,
        user: messages[i].user,
      });
    }
  };

  //   menampilkan data pesan dari firebase
  const getDataMessages = () => {
    database()
      .ref('messages')
      .limitToLast(20)
      .on('child_added', (snapshot) => {
        // console.log('isi dari snapshot->', snapshot);
        const value = snapshot.val();
        // console.log('isi dari value->', value);
        setMessages((previousMessages) =>
          GiftedChat.append(previousMessages, value),
        );
      });
  };
  return (
    <GiftedChat
      messages={messages}
      onSend={(messages) => onSend(messages)}
      user={{
        _id: user.uid,
        name: user.email,
        avatar:
          'https://store.playstation.com/store/api/chihiro/00_09_000/container/US/en/99/UP1675-CUSA11816_00-AV00000000000012//image?_version=00_09_000&platform=chihiro&w=720&h=720&bg_color=000000&opacity=100',
      }}
    />
  );
};

export default Chatting;
