import React, {useEffect, useState} from 'react';
import {Image, Text, View, StatusBar} from 'react-native';
import CardProfile from '../../components/CardProfile';
import AsyncStorage from '@react-native-community/async-storage';
import {GoogleSignin} from '@react-native-community/google-signin';

const Profile = ({navigation, route}) => {
  // console.log('Profile -> route', route);
  const [logged, setLogged] = useState(null);
  const [userInfo, setUserInfo] = useState(null);
  useEffect(() => {
    getCurrentUser();
    if (route.params !== undefined) {
      setLogged(route.params.logged);
    }
  }, []);
  const onLogoutPress = async () => {
    try {
      await AsyncStorage.removeItem('token');
      if (userInfo) {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      }
      navigation.reset({
        index: 0,
        routes: [{name: 'Login'}],
      });
    } catch (err) {
      console.log('logout error :', err);
    }
  };

  const getCurrentUser = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      console.log('isi userInfo', userInfo);
      setUserInfo(userInfo);
    } catch (error) {
      console.log(error);
    }
  };
  // const signOut = async () => {
  //   try {
  //     await AsyncStorage.removeItem('token');
  //     await GoogleSignin.revokeAccess();
  //     await GoogleSignin.signOut();
  //     navigation.reset({
  //       index: 0,
  //       routes: [{name: 'Login'}],
  //     });
  //   } catch (error) {
  //     console.error(error);
  //   }
  // };

  const logOut = () => {
    navigation.reset({
      index: 0,
      routes: [{name: 'Login'}],
    });
  };
  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBar barStyle="dark-content" />
      <View
        style={{
          backgroundColor: '#3ec6ff',
          height: 200,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={
            userInfo
              ? {uri: userInfo.user.photo}
              : require('../../assets/image/user.png')
          }
          style={{width: 100, height: 100, borderRadius: 50}}
        />
        <Text
          style={{
            fontWeight: 'bold',
            color: 'white',
            fontSize: 16,
            marginTop: 10,
          }}>
          {userInfo ? userInfo.user.name : 'Jhon'}
        </Text>
      </View>
      <View style={{alignItems: 'center', marginTop: -20}}>
        <CardProfile
          lahir="17 Agustus 1996"
          genre="Laki-laki"
          hobi="Ngoding"
          telp="0812 3456 789"
          email={userInfo ? userInfo.user.email : 'jhon@gmail.com'}
          onPress={() => (logged !== null ? logOut() : onLogoutPress())}
        />
      </View>
    </View>
  );
};

export default Profile;
