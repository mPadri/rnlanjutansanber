import React, {createContext, useState} from 'react';
import TodoList from './TodoList';

export const RootContext = createContext();

const Context = () => {
  const [todo, setTodo] = useState('');
  const [todoList, setTodoList] = useState([]);

  const date = new Date();
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  const handleChangeInput = (value) => {
    setTodo(value);
  };

  const addTodo = () => {
    let tanggal = `${day}/${month}/${year}`;

    // todoList.push(todo);
    setTodoList([...todoList, {title: todo, date: tanggal}]);
    setTodo('');
  };

  const deleteTodo = (index) => {
    setTodoList(todoList.filter((el, indexTodo) => indexTodo !== index));
    // console.log(index);
  };
  return (
    <RootContext.Provider
      value={{todo, todoList, handleChangeInput, addTodo, deleteTodo}}>
      <TodoList />
    </RootContext.Provider>
  );
};

export default Context;
