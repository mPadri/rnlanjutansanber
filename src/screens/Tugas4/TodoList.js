import React, {useContext} from 'react';
import {FlatList, Text, TextInput, TouchableOpacity, View} from 'react-native';
import ListTodo from '../../components/ListTodo';
import {RootContext} from './index';

const TodoList = () => {
  const state = useContext(RootContext);
  //   console.log('isi state global context:', state);

  //   console.log(todoList);
  return (
    <View style={{margin: 15}}>
      <Text>Masukan Todolist</Text>

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginVertical: 15,
        }}>
        <TextInput
          placeholder="Input here"
          style={{borderWidth: 1, width: '83%', height: 50}}
          value={state.todo}
          onChangeText={(value) => state.handleChangeInput(value)}
        />
        <TouchableOpacity
          style={{
            backgroundColor: '#3ec6ff',
            height: 50,
            width: 50,
            justifyContent: 'center',
          }}
          onPress={() => state.addTodo()}>
          <Text style={{alignSelf: 'center', fontSize: 24}}>+</Text>
        </TouchableOpacity>
      </View>

      <FlatList
        showsVerticalScrollIndicator={false}
        data={state.todoList}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <ListTodo
            key={index}
            date={item.date}
            title={item.title}
            onPress={() => state.deleteTodo(index)}
          />
        )}
      />
    </View>
  );
};

export default TodoList;
