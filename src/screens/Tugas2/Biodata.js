import React from 'react';
import {View, Text, Image, Dimensions, StatusBar} from 'react-native';

const Biodata = () => {
  return (
    <View>
      <StatusBar backgroundColor="#3ec6ff" />
      <View
        style={{
          backgroundColor: '#3ec6ff',
          height: 200,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={require('../../assets/image/profile.jpg')}
          style={{width: 100, height: 100, borderRadius: 50}}
        />
        <Text
          style={{
            fontWeight: 'bold',
            color: 'white',
            fontSize: 16,
            marginTop: 10,
          }}>
          Muhammad Padri
        </Text>
      </View>
      <View style={{alignItems: 'center', marginTop: -20}}>
        <View
          style={{
            backgroundColor: 'white',
            height: 210,
            width: Dimensions.get('window').width - 30,
            borderRadius: 10,
            elevation: 2,
          }}>
          <View style={{padding: 15}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 15,
              }}>
              <Text>Tanggal Lahir</Text>
              <Text>20 Desember 1991</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 15,
              }}>
              <Text>Jenis Kelamin</Text>
              <Text>Laki-laki</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 15,
              }}>
              <Text>Hobi</Text>
              <Text>Ngoding</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 15,
              }}>
              <Text>No. Telp</Text>
              <Text>0855 8824 286</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 10,
              }}>
              <Text>Email</Text>
              <Text>cvadhri@gmail.com</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Biodata;
