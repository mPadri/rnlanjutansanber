import React, {useState} from 'react';
import {FlatList, Text, TextInput, TouchableOpacity, View} from 'react-native';
import ListTodo from '../../components/ListTodo';

const TodoList = () => {
  const [todo, setTodo] = useState('');
  const [todoList, setTodoList] = useState([]);

  const date = new Date();
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  let tanggal = `${day}/${month}/${year}`;

  const addTodo = () => {
    todoList.push(todo);
    setTodo('');
  };

  //   console.log(todoList);
  return (
    <View style={{margin: 15}}>
      <Text>Masukan Todolist</Text>

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginVertical: 15,
        }}>
        <TextInput
          placeholder="Input here"
          style={{borderWidth: 1, width: '83%', height: 50}}
          value={todo}
          onChangeText={(value) => setTodo(value)}
        />
        <TouchableOpacity
          style={{
            backgroundColor: '#3ec6ff',
            height: 50,
            width: 50,
            justifyContent: 'center',
          }}
          onPress={addTodo}>
          <Text style={{alignSelf: 'center', fontSize: 24}}>+</Text>
        </TouchableOpacity>
      </View>

      <FlatList
        showsVerticalScrollIndicator={false}
        data={todoList}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <ListTodo
            key={index}
            date={tanggal}
            title={item}
            onPress={() =>
              setTodoList(
                todoList.filter((el, indexTodo) => indexTodo !== index),
              )
            }
          />
        )}
      />
    </View>
  );
};

export default TodoList;
