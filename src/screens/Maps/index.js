import React, {useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';

MapboxGL.setAccessToken(
  `pk.eyJ1IjoicGFkcmkiLCJhIjoiY2tkZmpwNTByMTUxcjJycXE1ajR4cHk3cSJ9.WJhXKJNLt0BQ7HqJ3fYgxw`,
);

const Maps = () => {
  useEffect(() => {
    const getLocation = async () => {
      try {
        const permission = await MapboxGL.requestAndroidLocationPermissions();
      } catch (error) {
        console.log(error);
      }
    };

    getLocation();
  }, []);

  const coordinates = [
    [107.598827, -6.896191],
    [107.596198, -6.899688],
    [107.618767, -6.902226],
    [107.621095, -6.89869],
    [107.615698, -6.896741],
    [107.613544, -6.897713],
    [107.613697, -6.893795],
    [107.610714, -6.891356],
    [107.605468, -6.893124],
    [107.60918, -6.898013],
  ];

  return (
    <View style={styles.container}>
      <MapboxGL.MapView style={styles.container}>
        <MapboxGL.UserLocation visible={true} />
        <MapboxGL.Camera followUserLocation={true} />
        {coordinates.map((coordinate, index) => {
          // console.log(coordinate[1]);
          return (
            <MapboxGL.PointAnnotation
              key={index}
              id={index.toString()}
              coordinate={coordinate}>
              <MapboxGL.Callout
                title={`Longitude: ${coordinate[0]} Latitude: ${coordinate[1]}`}
                key={index}
              />
            </MapboxGL.PointAnnotation>
          );
        })}
      </MapboxGL.MapView>
    </View>
  );
};

export default Maps;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
