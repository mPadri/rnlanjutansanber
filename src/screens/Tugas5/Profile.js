import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import {API} from '../../api';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';

const Profile = ({navigation}) => {
  const [userInfo, setUserInfo] = useState(null);

  useEffect(() => {
    // async function getToken() {
    //   // get token di localstorage yang di initial dengan key token
    //   try {
    //     const token = await AsyncStorage.getItem('token');
    //     return getVenue(token);
    //     // console.log('isi localstorage token ->', token);
    //   } catch (err) {
    //     console.log(err);
    //   }
    // }

    // getToken();
    getCurrentUser();
  }, [userInfo]);

  // const getVenue = (token) => {
  //   Axios.get(`${API}/venues`, {
  //     timeout: 20000,
  //     headers: {
  //       Authorization: 'Bearer' + token,
  //     },
  //   })
  //     .then((res) => {
  //       console.log('Profile -> response:', res.data);
  //     })
  //     .catch((err) => {
  //       console.log('Profile -> err:', err);
  //     });
  // };

  // const onLogoutPress = async () => {
  //   try {
  //     await AsyncStorage.removeItem('token');
  //     navigation.replace('Login');
  //   } catch (err) {
  //     console.log('logout error :', err);
  //   }
  // };

  const getCurrentUser = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      console.log('isi userInfo', userInfo);
      setUserInfo(userInfo);
    } catch (error) {
      console.log(error);
    }
  };
  const signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      navigation.replace('Login');
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <View>
      <StatusBar backgroundColor="#3ec6ff" />
      <View
        style={{
          backgroundColor: '#3ec6ff',
          height: 200,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={{uri: userInfo && userInfo.user && userInfo.user.photo}}
          style={{width: 100, height: 100, borderRadius: 50}}
        />
        <Text
          style={{
            fontWeight: 'bold',
            color: 'white',
            fontSize: 16,
            marginTop: 10,
          }}>
          {userInfo && userInfo.user && userInfo.user.name}
        </Text>
      </View>
      <View style={{alignItems: 'center', marginTop: -20}}>
        <View
          style={{
            backgroundColor: 'white',
            height: 250,
            width: Dimensions.get('window').width - 30,
            borderRadius: 10,
            elevation: 2,
          }}>
          <View style={{padding: 15}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 15,
              }}>
              <Text>Tanggal Lahir</Text>
              <Text>21 Juni 1997</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 15,
              }}>
              <Text>Jenis Kelamin</Text>
              <Text>Laki-laki</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 15,
              }}>
              <Text>Hobi</Text>
              <Text>Ngoding</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 15,
              }}>
              <Text>No. Telp</Text>
              <Text>082123456789</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 10,
              }}>
              <Text>Email</Text>
              <Text>{userInfo && userInfo.user && userInfo.user.email}</Text>
            </View>
            <View style={{marginTop: 10}}>
              <TouchableOpacity
                style={{
                  backgroundColor: '#3ec6ff',
                  padding: 10,
                  borderRadius: 2,
                }}
                onPress={() => navigation.replace('Login')}>
                <Text
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    color: '#fff',
                  }}>
                  LOG OUT
                </Text>
              </TouchableOpacity>
              {/* <TouchableOpacity
                style={{
                  backgroundColor: '#3ec6ff',
                  padding: 10,
                  borderRadius: 2,
                }}
                onPress={() => signOut()}>
                <Text
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    color: '#fff',
                  }}>
                  LOG OUT
                </Text>
              </TouchableOpacity> */}
              {/* <TouchableOpacity
                style={{
                  backgroundColor: '#3ec6ff',
                  padding: 10,
                  borderRadius: 2,
                }}
                onPress={() => onLogoutPress()}>
                <Text
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    color: '#fff',
                  }}>
                  LOG OUT
                </Text>
              </TouchableOpacity> */}
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Profile;
