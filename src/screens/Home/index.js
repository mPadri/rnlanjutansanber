import React, {useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  Dimensions,
  ScrollView,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Logo from '../../components/Logo';
import DetailSummary from '../../components/DetailSummary';
import {GoogleSignin} from '@react-native-community/google-signin';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const Home = ({navigation, route}) => {
  // console.log('Home -> route', route);
  useEffect(() => {
    configureGoogleSignIn();
  }, []);

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      webClientId:
        '5129955728-ev18qjsuea90qtsem0ncdr9e0hl3q30s.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
    });
  };
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <StatusBar barStyle="dark-content" backgroundColor="#fff" />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <View style={styles.card}>
            <View style={styles.wrapperTitle}>
              <Text style={styles.title}>Kelas</Text>
            </View>
            <View style={styles.wrapperLogo}>
              <Logo
                onPress={() => navigation.navigate('Chart')}
                type="Ionicons"
                logo="logo-react"
                namaLogo="React Native"
              />
              <Logo
                type="Ionicons"
                logo="logo-python"
                namaLogo="Data Science"
              />
              <Logo type="Ionicons" logo="logo-react" namaLogo="React Js" />
              <Logo type="Ionicons" logo="logo-laravel" namaLogo="Laravel" />
            </View>
          </View>
          <View style={styles.card}>
            <View style={styles.wrapperTitle}>
              <Text style={styles.title}>Kelas</Text>
            </View>
            <View style={styles.wrapperLogo}>
              <Logo
                type="Ionicons"
                logo="logo-wordpress"
                namaLogo="Wordpress"
              />
              <Logo
                type="Image"
                logo={require('../../assets/image/website-design.png')}
                namaLogo="Design Grafis"
              />
              <Logo
                type="MaterialCommunityIcons"
                logo="server"
                namaLogo="Web Server"
              />
              <Logo
                type="Image"
                logo={require('../../assets/image/ux.png')}
                namaLogo="UI/UX Design"
              />
            </View>
          </View>
          <View style={styles.cardSummary}>
            <View style={styles.wrapperTitle}>
              <Text style={styles.title}>Summary</Text>
            </View>
            <DetailSummary title="React Native" today="20" total="100" />
            <DetailSummary title="Data Science" today="30" total="100" />
            <DetailSummary title="React Js" today="66" total="100" />
            <DetailSummary title="Laravel" today="50" total="100" />
            <DetailSummary title="WordPress" today="30" total="100" />
            <DetailSummary title="Design Grafis" today="15" total="100" />
            <DetailSummary title="Web Server" today="45" total="100" />
            <DetailSummary title="UI/UX Design" today="56" total="100" />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Home;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  card: {
    backgroundColor: '#3EC6FF',
    width: windowWidth - 20,
    height: windowWidth / 3,
    marginVertical: 10,
    borderRadius: 8,
  },
  wrapperTitle: {
    backgroundColor: '#088dc4',
    borderTopRightRadius: 8,
    borderTopLeftRadius: 8,
    padding: 5,
  },
  title: {
    color: '#fff',
  },
  wrapperLogo: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 15,
  },
  cardSummary: {
    backgroundColor: '#3EC6FF',
    width: windowWidth - 20,
    height: windowHeight + 100,
    marginVertical: 10,
    borderRadius: 8,
  },
});
