import React from 'react';
import {Dimensions, Text, View, TouchableOpacity} from 'react-native';

const CardProfile = ({lahir, genre, hobi, telp, email, onPress}) => {
  return (
    <View
      style={{
        backgroundColor: 'white',
        height: 230,
        width: Dimensions.get('window').width - 30,
        borderRadius: 10,
        elevation: 2,
      }}>
      <View style={{padding: 15}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 15,
          }}>
          <Text>Tanggal Lahir</Text>
          <Text>{lahir}</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 15,
          }}>
          <Text>Jenis Kelamin</Text>
          <Text>{genre}</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 15,
          }}>
          <Text>Hobi</Text>
          <Text>{hobi}</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 15,
          }}>
          <Text>No. Telp</Text>
          <Text>{telp}</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 10,
          }}>
          <Text>Email</Text>
          <Text>{email}</Text>
        </View>
        <TouchableOpacity
          style={{
            backgroundColor: '#3ec6ff',
            padding: 10,
            borderRadius: 2,
          }}
          onPress={onPress}>
          <Text
            style={{
              textAlign: 'center',
              fontWeight: 'bold',
              color: '#fff',
            }}>
            LOG OUT
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CardProfile;
