import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const ListTodo = ({date, title, onPress}) => {
  return (
    <View
      style={{
        backgroundColor: 'white',
        padding: 10,
        borderWidth: 3,
        borderRadius: 4,
        borderColor: '#dedede',
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
      }}>
      <View>
        <Text>{date}</Text>
        <Text>{title}</Text>
      </View>
      <TouchableOpacity onPress={onPress}>
        <Icon name="trash-outline" size={24} />
      </TouchableOpacity>
    </View>
  );
};

export default ListTodo;
