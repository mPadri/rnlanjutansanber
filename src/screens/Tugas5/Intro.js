import React from 'react';
import {Image, StatusBar, StyleSheet, Text, View} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import Icon from 'react-native-vector-icons/Ionicons';

const slides = [
  {
    key: 'one',
    title: 'Belajar Intensif',
    text:
      '4 pekan online, 5 hari sepekan, estimasi materi dan tugas 3-4 jam per hari',
    image: require('../../assets/image/working-time.png'),
  },
  {
    key: 'two',
    title: 'Teknologi Populer',
    text: 'Menggunakan bahasa pemrograman populer',
    image: require('../../assets/image/research.png'),
  },
  {
    key: 'three',
    title: 'From Zero to Hero',
    text: 'Tidak ada syarat minimum skill, cocok untuk pemula',
    image: require('../../assets/image/venture.png'),
  },
  {
    key: 'four',
    title: 'Training Gratis',
    text: 'Kami membantu Anda mendapatkan pekerjaan / proyek',
    image: require('../../assets/image/money-bag.png'),
  },
];

const Intro = ({navigation}) => {
  const _renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
        <Image source={item.image} />
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  };

  const _oneDone = () => {
    navigation.replace('Login');
  };

  const _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon name="arrow-forward" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };
  const _renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon name="md-checkmark" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };

  return (
    <>
      <StatusBar barStyle="dark-content" backgroundColor="#fff" />
      <AppIntroSlider
        renderItem={_renderItem}
        data={slides}
        onDone={_oneDone}
        activeDotStyle={{backgroundColor: '#191970'}}
        renderNextButton={_renderNextButton}
        renderDoneButton={_renderDoneButton}
      />
    </>
  );
};

export default Intro;
const styles = StyleSheet.create({
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 15,
    color: '#191970',
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#d4d4d4',
    marginTop: 15,
    textAlign: 'center',
  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: '#191970',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
